#!/bin/sh
# 配置工具
# 根目录挂载 读写权限
mount -o remount,rw /
LYCIUM_CITOOLS_ROOT=`pwd`
# 创建 /CIusr 目录
USR="CIusr"
mkdir -p /data/$USR

ARCH=
# 获取测试设备cpu类型
cpubits=`getconf LONG_BIT`
if [ "${cpubits}" == "64" ]
then
   ARCH="arm64-v8a"
elif [ "${cpubits}" == "32" ]
then
   ARCH="armeabi-v7a"
else
   echo "ERROR CI DEVICE NOT SUPPORT!!!"
   exit 1
fi

# cputype=$1
# if [ "${cputype}" == "arm64" ]
# then
#     ARCH="arm64-v8a"
# elif [ "${cputype}" == "arm" ]
# then
#     ARCH="armeabi-v7a"
# else
#     echo "waring USE DEFAULT CI DEVICE arm64-v8a."
#     ARCH="arm64-v8a"
# fi

# lycium-citools
# 解压所有工具压缩包
# tar -zxf lyciumCI-$ARCH-busybox.tar.gz
# tar -zxf lyciumCI-$ARCH-cmake_make.tar.gz
# tar -zxf lyciumCI-$ARCH-perl.tar.gz
# tar -zxf lyciumCI-$ARCH-shell_cmd.tar.gz
for archive in $LYCIUM_CITOOLS_ROOT/lyciumCI-$ARCH-*.tar.gz
do
    tar -zxf $archive
done
# 拷贝 cmake make perl shell 工具二进制到 /data/$USR 目录
cp -rf $LYCIUM_CITOOLS_ROOT/$ARCH-cmake_make/* /data/$USR
# 拷贝 perl
cp -rfP $LYCIUM_CITOOLS_ROOT/perl/* /data/$USR
mv $LYCIUM_CITOOLS_ROOT/perl /data
cp -rfP $LYCIUM_CITOOLS_ROOT/$ARCH-shell_cmd/* /data/$USR
# 拷贝 busybox 工具到 /bin
cp -rf $LYCIUM_CITOOLS_ROOT/$ARCH-busybox/bin/busybox /data/$USR/bin

cd /data/$USR/bin
ln -fs /data/$USR/bin/busybox mkdir

cd /bin
# 设置 busybox 软连接命令
ln -fs /data/$USR/bin/busybox awk
ln -fs /data/$USR/bin/busybox expr
ln -fs /data/$USR/bin/busybox tr
ln -fs /data/$USR/bin/busybox diff
ln -fs /data/$USR/bin/bash bash
# 回到根目录
cd /
ln -fs /data/$USR /usr
# 验证
make -v
cmake --version
perl -v
diff --help

# 清理环境
# rm -rf $LYCIUM_CITOOLS_ROOT/lyciumCI-$ARCH-*.tar.gz \
#       $LYCIUM_CITOOLS_ROOT/$ARCH-cmake_make/ \
#       $LYCIUM_CITOOLS_ROOT/$ARCH-shell_cmd/ \
#       $LYCIUM_CITOOLS_ROOT/$ARCH-busybox/
# DONE!!! CI环境搭建完成
echo " SET CI env SUCCESS !!!"
