# Lycium CI tools
存放Lycium CI使用的工具

## 注意事项
x86 编译机器上的环境目录要和 ohos 测试运行的目录环境（包括三方库源码编译构建目录, cmake 目录, ctest 目录, make 目录）一致. 这样可以避免测试脚本目录环境不同造成的文件不存在错误. 如过测试环境没有的目录需要手动补齐.

    部分工具包较大, 执行
```
    sha512sum -c SHA512SUM
```
    进行文件校验

## AARCH64-linux-ohos 设备
arm64-v8a-cmake_make.tar.gz: 存放的是 aarch64 静态编译的 cmake 套件和 make 工具，可以在64位 ohos 设备上运行 make test, 和 ctest测试套.

arm64-v8a-busybox.tar.gz: 存放的是 aarch64 静态编译的 busybox 工具, 可以在64位 ohos 设备上运行 toybox 不支持的 sh 命令.

arm64-v8a-perl.tar.gz: 存放的是 aarch64 静态编译的 perl 工具及编译时使用的源码, 此工具包解压后直接放入 ohos 设备的 /data 目录使用（不要更换移动位置）.

arm64-v8a-shell_cmd.tar.gz: 存放的是 aarch64 静态编译的其他 shell 命令行工具.

## ARM-linux-ohos 设备
armeabi-v7a-cmake_make.tar.gz: 存放的是 arm 静态编译的 cmake 套件和 make 工具, 可以在32位 ohos 设备上运行make test, 和 ctest测试套.

armeabi-v7a-busybox.tar.gz: 存放的是 arm 静态编译的 busybox 工具, 可以在32位 ohos 设备上运行 toybox 不支持的 sh 命令.

armeabi-v7a-perl.tar.gz: 存放的是 arm 静态编译的 perl 工具及编译时使用的源码, 此工具包解压后直接放入 ohos 设备的 /data 目录使用（不要轻易移动位置）.

armeabi-v7a-shell_cmd.tar.gz: 存放的是 arm 静态编译的其他 shell 命令行工具.

## CI 环境搭建
### 自动搭建环境

setCIenv.sh 是一个自动配置 CI 环境的脚本, 该脚本必须和当前目录下的工具包在同一目录下, 将 CI 资源部推送到开发板并解压后只需执行以下命令即可:

```shell
# 将 lycium-citools 压缩上传到测试机, 使用 hdc 登录测试机, 解压 lycium-citools, 进入 lycium-citools 目录

sh setCIenv.sh          # 自动配置

```

执行成功后，在最后会出现`SET CI env SUCCESS !!!`字样。

### FAQ

## cmake 构建项目测试时可能出现的问题以及解决方案
cmake 及相关工具我们默认安装在 /usr/bin 路径下, 因为编译时使用的 cmake 是 ohos sdk 内的 cmake, 而运行 make test 是在测试机上，测试机不存在ohos sdk, 因此在运行 make test 时会出现, cmake 路径找不到的情况。

解决方案: 对于 cmake 编译的项目直接执行 ctest 测试, 不使用 "make test" 执行测试

![](./image/ctest_vs_make-test.png)

## busybox 支持的命令
busybox 支持的命令, 如果程序运行时报 cmd 找不到或者不识别. 优先在下表中查找看看 busybox 是否支持, 如果已经支持, 则可在 /bin 目录创建对应 cmd 的软连接.
### 示例
```
    # 假设 sed 命令报错
    # busybox 支持 sed
    cd /bin
    ln -fs busybox sed
    # 验证
    sed --version
```
![](./image/busybox_support_shell.png)

## shell_cmd 中支持的命令
![](./image/shell_cmd.png)

## 其他
[由 stesen 提供的 openharmony 工具](https://gitee.com/stesen/ohos_cross_tools)

